from typing import List

from walk_distance import WalkDistance

part_count = 1000


class MyMap:

    def __init__(self, state, seeds):
        self.seeds = seeds
        self.state = state

    def is_equal(self, other):
        return self.state == other

    def get_sum(self):
        return sum(self.seeds)


class WalkMap:

    def __init__(self, state, distance):
        self.state = state
        self.distance = distance

    def is_equal(self, other):
        return self.state == other

    def get_distance(self):
        return self.distance


class DisjointSet:

    def __init__(self, file):
        f = open(file, "r+")
        self.states: List[List[MyMap]] = []
        for i in range(part_count):
            self.states.append(list())
        for line in f:
            state = line[0:37]
            seeds = list()
            for item in line[38:-1].split(','):
                seeds.append(int(item))
            _ = state.__hash__()
            self.states[_ % part_count].append(MyMap(_, seeds))
        f.close()

    def find(self, state):
        temp_list = self.states[state % part_count]
        for item in temp_list:
            if item.state == state:
                return item.get_sum()
        return None


class WalkDistanceSet:

    def __init__(self, file):
        f = open(file, "r+")
        self.states: list = []
        for i in range(part_count):
            self.states.append(list())
        for line in f:
            state = line[0:56]
            distance = int(line[57:-1])
            _ = state.__hash__()
            self.states[_ % part_count].append(WalkMap(_, distance))
        f.close()

    def find(self, state: WalkDistance):
        row = state.state_to_hash()
        col = state.state_to_hash_col()
        sum = 0
        temp_list = self.states[row % part_count]
        for item in temp_list:
            if item.state == row:
                sum += item.get_distance()
        temp_list = self.states[col % part_count]
        for item in temp_list:
            if item.state == col:
                sum += item.get_distance()
        return sum
