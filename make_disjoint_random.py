import copy
import math
import random
from copy import deepcopy

from Puzzel_map import Action
from puzzel_core import *

max_depth = 20


class HereMap(PuzzleMap):

    @staticmethod
    def init(size, number_list, cost):
        if isinstance(number_list, list):
            for i in range(size):
                for j in range(size):
                    if number_list[i][j] == 0:
                        return HereMap(size - 1, number_list, (i, j), cost)
        else:
            temp = number_list.split(",")
            map = []
            count = 0
            for i in range(size):
                row = []
                for j in range(size):
                    row.append(int(temp[count]))
                    count += 1
                map.append(row)
            for i in range(size):
                for j in range(size):
                    if map[i][j] == 0:
                        return HereMap(size - 1, map, (i, j), cost)

    def __init__(self, size, map, space_pos, cost):
        super().__init__(size, map, space_pos, cost)
        self.seeds = []
        for i in range(4):
            self.seeds.append(0)

    def get_children(self):
        actions = self.get_valid_action()
        children = []
        for action in actions:
            children.append(self.move(action))
        return children

    def update_seeds(self, target):
        if target > 0:
            if target < 5:
                self.seeds[0] += 1
            else:
                if target < 9:
                    self.seeds[1] += 1
                else:
                    if target < 13:
                        self.seeds[2] += 1
                    else:
                        self.seeds[3] += 1

    def move(self, action):
        out = copy.deepcopy(self)
        out.actions_until_now.append(action)
        out.cost += 1
        x, y = Action.get_action_value(action)
        self.update_seeds(out.map[out.space_pos[0] + x][out.space_pos[1] + y])
        out.map[out.space_pos[0]][out.space_pos[1]] = out.map[out.space_pos[0] + x][out.space_pos[1] + y]
        out.map[out.space_pos[0] + x][out.space_pos[1] + y] = 0
        out.space_pos = (out.space_pos[0] + x, out.space_pos[1] + y)
        return out

    def my_move(self, action):
        out = self
        x, y = Action.get_action_value(Action.get_reverse_action(action))
        temp = 0
        if out.map[out.space_pos[0] + x][out.space_pos[1] + y] != -1:
            temp += 1
        out.map[out.space_pos[0]][out.space_pos[1]] = out.map[out.space_pos[0] + x][out.space_pos[1] + y]
        out.map[out.space_pos[0] + x][out.space_pos[1] + y] = 0
        out.space_pos = (out.space_pos[0] + x, out.space_pos[1] + y)
        return out, temp

    @staticmethod
    def make_random_state(size):
        numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        out = ""
        for i in range(size):
            for j in range(size):
                temp = numbers.pop(math.floor(random.random() * len(numbers)))
                out += str(temp)
                out += ","
        return HereMap.init(size, out[0:-1], 0)

    @staticmethod
    def make_final_state(map_size):
        map = []
        count = 1
        for i in range(map_size):
            row = []
            for j in range(map_size):
                row.append(count)
                count += 1
            map.append(row)
        map[-1][-1] = 0
        return HereMap(map_size - 1, map, (map_size - 1, map_size - 1), 0)

    def state_to_data_string(self):
        out = ""
        for row in self.map:
            for item in row:
                out += str(item)
                out += str(',')
        out = out[0:-1]
        out += '-'
        for item in self.seeds:
            out += str(item)
            out += str(',')
        return out[0:-1]

    def make_star(self, numbers: list):
        out = deepcopy(self)
        for i in range(out.size + 1):
            for j in range(out.size + 1):
                if not numbers.__contains__(out.map[i][j]):
                    out.map[i][j] = -1
        return out

    def is_final(self):
        if self.map[self.size][self.size] != 0:
            return False
        check = 1
        for row in self.map:
            for item in row:
                if item != check:
                    if item != 0 and item != -1:
                        return False
                check += 1

        return True

    def state_to_string(self):
        out = ""
        for row in self.map:
            for item in row:
                if item == -1:
                    out += "*"
                else:
                    out += str(item)
                out += str(',')
        return out[0:-1]

    def state_to_hash(self):
        return self.state_to_string().__hash__()

    def is_equal(self, state):
        if state.size != self.size:
            return False
        return state.map == self.map


def make_it_3(puzzle: HereMap):
    one = puzzle.make_star([1, 2, 3, 4, 5, 0])
    two = puzzle.make_star([6, 7, 8, 9, 10, 0])
    three = puzzle.make_star([11, 12, 13, 14, 15, 0])
    return one, two, three


def manhattan_star(state):
    count = 1
    distance_sum = 0
    for i in range(state.size + 1):
        for j in range(state.size + 1):
            if state.map[i][j] != count:
                if state.map[i][j] != 0 and state.map[i][j] != -1:
                    column = (state.map[i][j] % (state.size + 1))
                    if column == 0:
                        column = 3
                    column -= 1
                    row = math.floor((state.map[i][j] - column) / (state.size + 1))
                    distance_sum += (abs(column - j) + abs(row - i))
            count += 1
    return distance_sum


def contains_hash(state_hash, visited_state):
    for item in visited_state:
        if state_hash == item:
            return True
    return False


def bfs(fringe):
    while len(fringe) != 0:
        return fringe.pop(0)
    return None


def solve_with_star(solver, init_state):
    if init_state.is_final():
        return True

    solver.visited_state.append(init_state.state_to_hash())
    fringe = PriorityQueue()
    for child in init_state.get_children():
        child.heuristic = manhattan_star(child)
        fringe.put(child)

    while True:
        if fringe.empty():
            return False
        next_state = solver.a_star(fringe)
        if next_state is None:
            return False
        if next_state.is_final():
            print(fringe.qsize())
            solver.ans_node = next_state
            return True
        for child in next_state.get_children():
            if not solver.contains_hash(child.state_to_hash()):
                child.heuristic = manhattan_star(child)
                fringe.put(child)


def make_state_backward(state: HereMap, actions_count):
    output = [state.state_to_string() + "-" + str(actions_count)]
    for action in reversed(state.actions_until_now):
        state, temp = state.my_move(action)
        actions_count = actions_count - temp
        output.append(state.state_to_string() + "-" + str(actions_count))
    return output


if __name__ == '__main__':
    i = 0
    size = 4
    data = [[], [], []]
    while i < 1:
        init_state = HereMap.make_random_state(size)
        if init_state.is_solvable():
            j = 0
            for temp_map in make_it_3(init_state):
                solver = PuzzleSolver()
                solve_with_star(solver, temp_map)
                data[0].append(make_state_backward(solver.ans_node, solver.ans_node.seeds[j]))
        else:
            print("unsolvable")
        i += 1
    print(data)
    print(data[0])
    print(data[1])
    print(data[2])
