import copy
import enum

import math


class WalkPuzzleMap:
    @staticmethod
    def init(size, number_list):
        if isinstance(number_list, list):
            for i in range(size):
                for j in range(size):
                    if number_list[i][j] == 0:
                        return WalkPuzzleMap(size - 1, number_list)
        else:
            temp = number_list.split(",")
            map = []
            count = 0
            for i in range(size):
                row = []
                for j in range(size):
                    row.append(int(temp[count]))
                    count += 1
                map.append(row)
            for i in range(size):
                for j in range(size):
                    if map[i][j] == 0:
                        return WalkPuzzleMap(size - 1, map)

    def __init__(self, size, map):
        self.size = size
        self.map = map


class WalkDistance:

    def __init__(self, state: WalkPuzzleMap):
        self.state = state
        self.space = (0, 0)
        self.table = self.row_table()
        self.table_col = self.col_table()
        self.depth = 0

    def col_table(self):
        table = list()
        for i in range(self.state.size + 1):
            table.append([0, 0, 0, 0])
        for i in range(self.state.size + 1):
            for j in range(self.state.size + 1):
                if self.state.map[i][j] != 0:
                    column = (self.state.map[i][j] % (self.state.size + 1))
                    if column == 0:
                        column = 4
                    column -= 1
                    table[j][column] += 1
                else:
                    self.space = (i, j)
        return table

    def row_table(self):
        table = list()
        for i in range(self.state.size + 1):
            table.append([0, 0, 0, 0])
        for i in range(self.state.size + 1):
            for j in range(self.state.size + 1):
                if self.state.map[i][j] != 0:
                    column = (self.state.map[i][j] % (self.state.size + 1))
                    if column == 0:
                        column = 4
                    column -= 1
                    row = math.floor((self.state.map[i][j] - column) / (self.state.size + 1))
                    table[i][row] += 1
                else:
                    self.space = (i, j)
        return table

    def get_valid_actions(self, row):
        if row:
            if self.space[0] == 0:
                ans = set()
                for i in range(len(self.table[1])):
                    if self.table[1][i] > 0:
                        ans.add((WalkAction.Down, i))
                return ans
            if self.space[0] == self.state.size:
                ans = set()
                for i in range(len(self.table[self.state.size - 1])):
                    if self.table[self.state.size - 1][i] > 0:
                        ans.add((WalkAction.Up, i))
                return ans
            ans = set()
            for i in range(len(self.table[self.space[0] - 1])):
                if self.table[self.space[0] - 1][i] > 0:
                    ans.add((WalkAction.Up, i))
            for i in range(len(self.table[self.space[0] - 1])):
                if self.table[self.space[0] + 1][i] > 0:
                    ans.add((WalkAction.Down, i))
            return ans
        if not row:
            return None

    def is_final(self, row):
        if self.table[0][0] == 4:
            if self.table[1][1] == 4:
                if self.table[2][2] == 4:
                    if self.table[3][3] == 3:
                        return True
        return False

    def move(self, action: tuple, row):
        out = copy.deepcopy(self)
        if row:
            if action[0] == WalkAction.Up:
                out.table[self.space[0] - 1][action[1]] -= 1
                out.table[self.space[0]][action[1]] += 1
                out.space = (out.space[0] - 1, out.space[1])
            if action[0] == WalkAction.Down:
                out.table[self.space[0] + 1][action[1]] -= 1
                out.table[self.space[0]][action[1]] += 1
                out.space = (out.space[0] + 1, out.space[1])
        else:
            pass
        out.depth += 1
        return out

    def state_to_data_string(self):
        out = str(self.table)
        out += "-"
        out += str(self.depth)
        return out

    def get_children(self, row):
        children = []
        for action in self.get_valid_actions(row):
            children.append(self.move(action, row))
        return children

    def state_to_hash(self):
        return str(self.table).__hash__()

    def state_to_hash_col(self):
        return str(self.table_col).__hash__()


class WalkAction(enum.Enum):
    Up = 1
    Down = 2


visited = []
data = []
max_depth = 3
visited_part = 1000


def contains(temp_state):
    for state in visited[temp_state % visited_part]:
        if state == temp_state:
            return True
    return False


def add_visited(temp_state):
    visited[temp_state % visited_part].append(temp_state)


def move_on_bfs(start: WalkDistance, row):
    for i in range(visited_part):
        visited.append([])

    fringe = [start]
    data.append(start.state_to_data_string())

    while len(fringe) != 0:
        state = fringe.pop(0)
        if not contains(state.state_to_hash()):
            add_visited(state.state_to_hash())
            data.append(state.state_to_data_string())
        else:
            continue
        for child in state.get_children(row):
            if not contains(child.state_to_hash()):
                fringe.append(child)


if __name__ == '__main__':
    # start_state = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,0'
    # start_state = '12,1,10,2,7,11,4,14,5,0,9,15,8,13,6,3'
    start_state = '2,6,7,12,5,13,10,4,0,8,3,15,9,1,14,11'
    puzzle = WalkPuzzleMap.init(4, start_state)
    walkDis = WalkDistance(puzzle)
    print(walkDis.table)
    print(walkDis.table_col)
    # print(PuzzleMap.walk_distance(puzzle))
    # move_on_bfs(walkDis, True)
    # print(len(data))
    # f = open('walk_distance_rows.txt', '+w')
    # for item in data:
    #     f.writelines([item, "\n"])
    # f.close()
