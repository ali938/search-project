from operator import attrgetter
from queue import PriorityQueue
from time import time

from Puzzel_map import PuzzleMap


class PuzzleSolver:

    def __init__(self):
        self.visited_state = []
        self.visited_part = 1000
        for i in range(self.visited_part):
            self.visited_state.append([])
        self.visited_state_direction = []
        self.dfs_max_cost = 14
        self.current_max_cost = 1
        self.ans_node: PuzzleMap = None

    def add_to_visited(self, state):
        self.visited_state[state % self.visited_part].append(state)

    def solve(self, next_state_selector, init_state):
        if init_state.is_final():
            return True

        if not init_state.is_solvable():
            print("unsolvable")
            return False

        self.add_to_visited(init_state.state_to_hash())
        fringe = PriorityQueue()
        for child in init_state.get_children():
            fringe.put(child)

        while True:
            if fringe.empty():
                return False
            next_state = next_state_selector(self, fringe)
            if next_state is None:
                return False
            if next_state.is_final():
                print(next_state.actions_until_now)
                return True
            for child in next_state.get_children():
                if not self.contains_hash(child):
                    fringe.put(child)

    def solve_heuristic(self, next_state_selector, heuristic, init_state):
        if init_state.is_final():
            return True

        if not init_state.is_solvable():
            print("unsolvable")
            return False

        self.add_to_visited(init_state.state_to_hash())
        fringe = PriorityQueue()
        for child in init_state.get_children():
            child.heuristic = heuristic(child)
            fringe.put(child)

        while True:
            if fringe.empty():
                return False
            next_state = next_state_selector(fringe)
            if next_state is None:
                return False
            if next_state.is_final():
                print(fringe.qsize())
                self.ans_node = next_state
                return True
            for child in next_state.get_children():
                if not self.contains_hash(child.state_to_hash()):
                    child.heuristic = heuristic(child)
                    fringe.put(child)

    def solve_bidirectional(self, init_state):
        if init_state.is_final():
            return True

        if not init_state.is_solvable():
            print("unsolvable")
            return False

        fringe = init_state.get_children()
        fringe_last = [PuzzleMap.make_final_state(init_state.size + 1)]

        direction = True
        while True:
            if len(fringe) == 0 or len(fringe_last) == 0:
                return False
            if direction:
                temp_state = fringe.pop(0)
                if not self.contains_bidirectional(temp_state, direction):
                    for state in fringe_last:
                        if temp_state.state == state:
                            print(temp_state.actions_until_now)
                            print(state.actions_until_now)
                            return True
                    direction = not direction
                    for child in temp_state.get_children():
                        if not self.contains_bidirectional(child, direction):
                            fringe.append(child)
            else:
                temp_state = fringe_last.pop(0)
                if not self.contains_bidirectional(temp_state, direction):
                    for state in fringe:
                        if temp_state.state == state:
                            print(temp_state.actions_until_now)
                            print(state.actions_until_now)
                            return True
                    direction = not direction
                    for child in temp_state.get_children():
                        if not self.contains_bidirectional(child, direction):
                            fringe_last.append(child)

    def solve_heuristic_ida(self, heuristic, init_state):
        if init_state.is_final():
            return True

        if not init_state.is_solvable():
            print("unsolvable")
            return False

        while True:
            self.visited_state = []
            for i in range(self.visited_part):
                self.visited_state.append([])
            self.add_to_visited(init_state.state_to_hash())
            fringe = init_state.get_children()
            for child in fringe:
                child.heuristic = heuristic(child)

            while True:
                if len(fringe) == 0:
                    return False
                next_state = self.ida_star(fringe)
                if next_state is None:
                    self.current_max_cost += 1
                    break
                if next_state.is_final():
                    self.ans_node = next_state
                    return True
                for child in next_state.get_children():
                    if not self.contains_hash(child.state_to_hash()):
                        child.heuristic = heuristic(child)
                        fringe.append(child)

    def bfs(self, state_list):
        while len(state_list) != 0:
            temp_state = state_list.pop(0)
            if not self.contains_hash(temp_state.state_to_hash()):
                self.add_to_visited(temp_state.state_to_hash())
                return temp_state
        return None

    def dfs(self, state_list):
        while len(state_list) != 0:
            temp_state = state_list.pop(-1)
            if not self.contains_hash(temp_state.state_to_hash()):
                if temp_state.cost <= self.dfs_max_cost:
                    self.add_to_visited(temp_state.state_to_hash())
                    return temp_state
        return None

    def ucs(self, state_list):
        while len(state_list) != 0:
            temp_state = min(state_list, key=attrgetter('cost'))
            state_list.remove(temp_state)
            if not self.contains_hash(temp_state.state_to_hash()):
                self.add_to_visited(temp_state.state_to_hash())
                return temp_state
        return None

    def ida_star(self, state_list):
        min_state = None
        min_state_value = +10000000
        for state in state_list:
            cost = state.cost
            pred_cost = state.heuristic
            if min_state_value > (cost + pred_cost):
                min_state_value = (cost + pred_cost)
                min_state = state

        if min_state.cost > self.current_max_cost:
            return None

        self.add_to_visited(min_state.state_to_hash())
        state_list.remove(min_state)
        return min_state

    def a_star(self, state_list: PriorityQueue):
        # min_state = None
        # min_state_value = +10000000
        # min_state_heuristic = +10000000
        # for state in state_list:
        #     cost = state.cost
        #     pred_cost = state.heuristic
        #     if min_state_value > (cost + pred_cost):
        #         min_state_value = (cost + pred_cost)
        #         min_state_heuristic = pred_cost
        #         min_state = state
        #     else:
        #         if min_state_value == (cost + pred_cost):
        #             if min_state_heuristic > pred_cost:
        #                 min_state = state
        #                 min_state_heuristic = pred_cost
        min_state = state_list.get()
        self.add_to_visited(min_state.state_to_hash())
        return min_state

    def contains_bidirectional(self, state, direction):
        for item in self.visited_state if direction else self.visited_state_direction:
            if state.state == item:
                return True
        return False

    def contains_hash(self, state):
        return state in self.visited_state[state % self.visited_part]

    def visited_state_count(self):
        sum = 0
        for i in self.visited_state:
            sum += len(i)
        return sum


if __name__ == '__main__':
    size = 4
    # start_state = '1,0,2,4,3,5,6,7,8,9,10,11,12,13,14,15' # their sample
    # start_state = '1,2,3,4,5,6,7,8,9,10,11,12,13,0,14,15' # depth 2
    # start_state = '5,1,2,3,9,7,11,4,13,6,15,8,14,10,0,12'  # depth 15
    # start_state = '5,1,6,4,12,10,2,8,9,11,0,3,13,15,14,7' # depth 30
    # start_state = '1,11,2,0,10,9,6,4,3,8,7,12,13,14,15,5'  # depth 35
    # start_state = '1,4,8,11,15,3,7,10,13,0,5,9,2,14,12,6'  # depth 39
    # start_state = '5,1,4,10,12,11,6,0,13,9,2,8,15,14,7,3'  # depth 44
    start_state = '9,2,7,14,11,8,10,13,1,6,0,3,15,5,12,4' # depth 50
    # start_state = '5,14,3,0,4,15,6,13,9,2,10,1,12,11,7,8' # depth 55

    # before = time()
    # start = PuzzleMap.init(size, start_state, 0)
    # model = PuzzleSolver()
    # model.solve_bidirectional(start)
    # print("visited node: " + str(model.visited_state_count()))
    # print("spent time:" + str(time() - before))
    #
    # before = time()
    # model = PuzzleSolver()
    # start = PuzzleMap.init(size, start_state, 0)
    # model.solve(PuzzleSolver.bfs, start)
    # print("visited node: " + str(model.visited_state_count()))
    # print("spent time:" + str(time() - before))
    #
    # before = time()
    # model = PuzzleSolver()
    # start = PuzzleMap.init(size, start_state, 0)
    # model.solve(PuzzleSolver.dfs, start)
    # print("visited node: " + str(model.visited_state_count()))
    # print("spent time:" + str(time() - before))

    # before = time()
    # start = PuzzleMap.init(size, start_state, 0)
    # model = PuzzleSolver()
    # model.solve_heuristic(model.a_star, PuzzleMap.manhattan_distance, start)
    # print(model.ans_node.actions_until_now)
    # print("actions count:" + str(len(model.ans_node.actions_until_now)))
    # print("visited node: " + str(model.visited_state_count()))
    # print("spent time:" + str(time() - before))
    # print()
    # model.ans_node.actions_to_state_file("sample.txt")

    before = time()
    start = PuzzleMap.init(size, start_state, 0)
    model = PuzzleSolver()
    model.solve_heuristic(model.a_star, PuzzleMap.linear_manhattan, start)
    print(model.ans_node.actions_until_now)
    print("actions count:" + str(len(model.ans_node.actions_until_now)))
    print("visited node: " + str(model.visited_state_count()))
    print("spent time:" + str(time() - before))
    print()
    # #
    # before = time()
    # start = PuzzleMap.init(size, start_state, 0)
    # model = PuzzleSolver()
    # model.solve_heuristic(model.a_star, PuzzleMap.disjoint_dataset, start)
    # print(model.ans_node.actions_until_now)
    # print("actions count:" + str(len(model.ans_node.actions_until_now)))
    # print("visited node: " + str(model.visited_state_count()))
    # print("spent time:" + str(time() - before))
    # print()
    #
    # before = time()
    # start = PuzzleMap.init(size, start_state, 0)
    # model = PuzzleSolver()
    # model.solve_heuristic_ida(PuzzleMap.manhattan_distance, start)
    # print(model.ans_node.actions_until_now)
    # print("actions count:" + str(len(model.ans_node.actions_until_now)))
    # print("visited node: " + str(model.visited_state_count()))
    # print("spent time:" + str(time() - before))
    # print()
    #
    # before = time()
    # start = PuzzleMap.init(size, start_state, 0)
    # model = PuzzleSolver()
    # model.solve_heuristic_ida(PuzzleMap.linear_manhattan, start)
    # print(model.ans_node.actions_until_now)
    # print("actions count:" + str(len(model.ans_node.actions_until_now)))
    # print("visited node: " + str(model.visited_state_count()))
    # print("spent time:" + str(time() - before))
    # print()
    #
    # before = time()
    # start = PuzzleMap.init(size, start_state, 0)
    # model = PuzzleSolver()
    # model.solve_heuristic_ida(PuzzleMap.disjoint_dataset, start)
    # print(model.ans_node.actions_until_now)
    # print("actions count:" + str(len(model.ans_node.actions_until_now)))
    # print("visited node: " + str(model.visited_state_count()))
    # print("spent time:" + str(time() - before))
    # print()

    before = time()
    start = PuzzleMap.init(size, start_state, 0)
    model = PuzzleSolver()
    model.solve_heuristic(model.a_star, PuzzleMap.walk_distance, start)
    print(model.ans_node.actions_until_now)
    print("actions count:" + str(len(model.ans_node.actions_until_now)))
    print("visited node: " + str(model.visited_state_count()))
    print("spent time:" + str(time() - before))
    print()
    # model.ans_node.actions_to_state_file("50_move.txt")
    # before = time()
    # start = PuzzleMap.init(size, start_state, 0)
    # model = PuzzleSolver()
    # model.solve_heuristic_ida(PuzzleMap.walk_distance, start)
    # print(model.ans_node.actions_until_now)
    # print("actions count:" + str(len(model.ans_node.actions_until_now)))
    # print("visited node: " + str(model.visited_state_count()))
    # print("spent time:" + str(time() - before))
    # print()
