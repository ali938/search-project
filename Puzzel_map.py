import enum
import math
from copy import deepcopy

from disjoint_set import WalkDistanceSet
from walk_distance import WalkDistance

# files = ['disjoint_one.txt', 'disjoint_two.txt', 'disjoint_three.txt', 'disjoint_four.txt']
# disjoint_set_obj = DisjointSet(files)
walk_distance_obj = WalkDistanceSet('walk_distance_rows.txt')


class PuzzleMap:

    @staticmethod
    def init(size, number_list, cost):
        if isinstance(number_list, list):
            for i in range(size):
                for j in range(size):
                    if number_list[i][j] == 0:
                        return PuzzleMap(size - 1, number_list, (i, j), cost)
        else:
            temp = number_list.split(",")
            map = []
            count = 0
            for i in range(size):
                row = []
                for j in range(size):
                    row.append(int(temp[count]))
                    count += 1
                map.append(row)
            for i in range(size):
                for j in range(size):
                    if map[i][j] == 0:
                        return PuzzleMap(size - 1, map, (i, j), cost)

    def __init__(self, size, map, space_pos, cost):
        self.size = size
        self.map = map
        self.space_pos = space_pos
        self.cost = cost
        self.heuristic = 0
        self.actions_until_now = []

    def get_valid_action(self):
        actions = []
        if self.space_pos[0] != 0:
            actions.append(Action.Up)
        if self.space_pos[0] != self.size:
            actions.append(Action.Down)
        if self.space_pos[1] != 0:
            actions.append(Action.Left)
        if self.space_pos[1] != self.size:
            actions.append(Action.Right)
        return actions

    def copy_map(self):
        copy_map = []
        for row in self.map:
            copy_map.append(row.copy())
        return copy_map

    # def copy_actions(self):
    #     copy_actions = []
    #     for action in self.actions_until_now:
    #         copy_actions.append(action.copy())
    #     return copy_actions

    def move(self, action):
        out_map = self.copy_map()
        out = PuzzleMap(self.size, out_map, self.space_pos, self.cost + 1)
        out.actions_until_now = self.actions_until_now.copy()
        out.actions_until_now.append(action)
        x, y = Action.get_action_value(action)
        out.map[out.space_pos[0]][out.space_pos[1]] = out.map[out.space_pos[0] + x][out.space_pos[1] + y]
        out.map[out.space_pos[0] + x][out.space_pos[1] + y] = 0
        out.space_pos = (out.space_pos[0] + x, out.space_pos[1] + y)
        return out

    def to_one_d_array(self):
        temp = []
        for row in self.map:
            temp.extend(row)
        return temp

    def is_final(self):
        check = 1
        for row in self.map:
            for item in row:
                if item != check:
                    if item != 0:
                        return False
                check += 1
        return True

    def state_to_string_old(self):
        out = ""
        for row in self.map:
            for item in row:
                out += f'{item},'
        return out[:-1]

    def state_to_string(self):
        return str(self.map)

    def state_to_string_space(self):
        out = ""
        for row in self.map:
            for item in row:
                out += str(item)
                out += str(', ')
        return out[0:-2]

    def state_to_hash(self):
        return self.state_to_string().__hash__()

    def state_to_old_hash(self):
        return self.state_to_string_old().__hash__()

    def is_equal(self, state):
        if state.size != self.size:
            return False
        return state.map == self.map

    def get_children(self):
        actions = self.get_valid_action()
        children = []
        for action in actions:
            children.append(self.move(action))
        return children

    def actions_to_state_file(self, file_name):
        f = open(file_name, '+w')
        state_str = []
        temp_state = PuzzleMap.make_final_state(self.size + 1)
        state_str.append(temp_state.state_to_string_space())
        for action in reversed(self.actions_until_now):
            temp_state = temp_state.move(Action.get_reverse_action(action))
            state_str.append(temp_state.state_to_string_space())

        f.writelines([str(self.size + 1), "\n"])
        for i in reversed(state_str):
            f.writelines([i, "\n"])
        f.close()

    def __lt__(self, other):
        _ = (self.heuristic + self.cost) - (other.heuristic + other.cost)
        if _ < 0:
            return True
        return False

    def __gt__(self, other):
        _ = (self.heuristic + self.cost) - (other.heuristic + other.cost)
        if _ > 0:
            return True
        return False

    def __eq__(self, other):
        if (self.heuristic + self.cost) - (other.heuristic + other.cost) == 0:
            return True
        return False

    @staticmethod
    def make_final_state(map_size):
        map = []
        count = 1
        for i in range(map_size):
            row = []
            for j in range(map_size):
                row.append(count)
                count += 1
            map.append(row)
        map[-1][-1] = 0
        return PuzzleMap(map_size - 1, map, (map_size - 1, map_size - 1), 0)

    def is_solvable(self):
        inversions = PuzzleMap.inversion_count_remove_zero(self.to_one_d_array())
        temp = self.size + 1 - self.space_pos[0] + 1
        if (self.size + 1) % 2 == 1:
            if inversions % 2 == 0:
                return True
        else:
            if temp % 2 == 1:
                if inversions % 2 == 1:
                    return True
            else:
                if inversions % 2 == 0:
                    return True
        return False

    @staticmethod
    def disjoint_dataset(state):
        return disjoint_set_obj.find(state)

    @staticmethod
    def linear_manhattan(state):
        return PuzzleMap.manhattan_distance(state) + (2 * PuzzleMap.linear_conflicts(state))

    @staticmethod
    def manhattan_distance(state):
        count = 1
        distance_sum = 0
        temp_size = state.size + 1
        for i in range(temp_size):
            for j in range(temp_size):
                if state.map[i][j] != count:
                    if state.map[i][j] != 0:
                        column = (state.map[i][j] % (state.size + 1))
                        if column == 0:
                            column = temp_size
                        column -= 1
                        row = math.floor((state.map[i][j] - column) / (state.size + 1))
                        distance_sum += (abs(column - j) + abs(row - i))
                count += 1
        return distance_sum

    @staticmethod
    def linear_conflicts(state):
        conflicts_sum = 0
        for i, row in enumerate(state.map):
            max_of_row = i * len(state.map) + len(state.map)
            min_of_row = i * len(state.map) + 1
            temp_row = []
            for item in row:
                if max_of_row >= item >= min_of_row:
                    temp_row.append(item)
            print(temp_row)
            conflicts_sum += PuzzleMap.inversion_count(temp_row)

        count = 1
        for j in range(state.size + 1):
            column = []
            for i in range(state.size + 1):
                if state.map[i][j] % (state.size + 1) == count:
                    if state.map[i][j] == 0:
                        continue
                    column.append(state.map[i][j])
            count += 1
            count = count % (state.size + 1)
            print(column)
            conflicts_sum += PuzzleMap.inversion_count(column)

        return conflicts_sum

    @staticmethod
    def inversion_count_remove_zero(row: list):
        if row.__contains__(0):
            row.remove(0)
        if len(row) < 2:
            return 0
        else:
            temp, row = PuzzleMap.sub_inversion_count(row)
            return temp

    @staticmethod
    def inversion_count(row: list):
        if len(row) < 2:
            return 0
        return PuzzleMap.sub_inversion_count(row)[0]

    @staticmethod
    def connect_two_list(left, right):
        i = 0
        j = 0
        temp = []
        inversion_count = 0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                temp.append(left[i])
                i += 1
            else:
                temp.append(right[j])
                j += 1
                inversion_count += (len(left) - i)

        while i < len(left):
            temp.append(left[i])
            i += 1

        while j < len(right):
            temp.append(right[j])
            j += 1

        return inversion_count, temp

    @staticmethod
    def sub_inversion_count(row: list):
        if len(row) <= 1:
            return 0, row
        left = row[0:math.ceil(len(row) / 2)]
        right = row[math.ceil(len(row) / 2):len(row)]
        count_left, left = PuzzleMap.sub_inversion_count(left)
        count_right, right = PuzzleMap.sub_inversion_count(right)
        inversion_count, row = PuzzleMap.connect_two_list(left, right)
        inversion_count += count_left + count_right
        return inversion_count, row

    @staticmethod
    def walk_distance(state):
        temp = walk_distance_obj.find(WalkDistance(state))
        other_temp = PuzzleMap.linear_manhattan(state)
        return max(temp, other_temp)

    def make_star(self, numbers: list):
        out = deepcopy(self)
        for i in range(out.size + 1):
            for j in range(out.size + 1):
                if not numbers.__contains__(out.map[i][j]):
                    out.map[i][j] = -1
        return out


class Action(enum.Enum):
    Up = 1
    Down = 2
    Left = 3
    Right = 4

    @staticmethod
    def get_action_value(action):
        if action == Action.Up:
            return -1, 0
        if action == Action.Down:
            return 1, 0
        if action == Action.Left:
            return 0, -1
        if action == Action.Right:
            return 0, 1

    @staticmethod
    def get_reverse_action(action):
        if action == Action.Up:
            return Action.Down
        if action == Action.Down:
            return Action.Up
        if action == Action.Left:
            return Action.Right
        if action == Action.Right:
            return Action.Left


if __name__ == '__main__':
    # start_state = '3,6,4,8,2,11,7,15,5,9,12,14,10,13,1,0'
    # start_state = '12,1,10,2,7,11,4,14,5,0,9,15,8,13,6,3'
    start_state = '1,2,3,4,15,6,7,8,9,10,11,12,13,14,5,16,17,18,19,20,21,22,23,24,0'
    puzzle = PuzzleMap.init(5, start_state, 0)
    print(puzzle.is_solvable())
    print(puzzle.is_final())
    print(puzzle.move(Action.Up).state_to_string())
    print(PuzzleMap.manhattan_distance(puzzle.move(Action.Up)))
    print(PuzzleMap.linear_conflicts(puzzle))
# start_state = '2,6,7,12,5,13,10,4,0,8,3,15,9,1,14,11'
# puzzle = WalkPuzzleMap.init(4, start_state)
# walkDis = WalkDistance(puzzle)
# print(walkDis.table_col)
# print(walkDis.table)
# print(PuzzleMap.walk_distance(puzzle))
# rows, cols = PuzzleMap.disjoint_dataset(PuzzleMap.init(4, start_state, 0))
# for row in rows:
#     print(row)
# print()
# for row in cols:
#     print(row)
