from make_disjoint_random import HereMap


def make_it_4(puzzle: HereMap):
    one = puzzle.make_star([1, 2, 3, 4, 0])
    two = puzzle.make_star([5, 6, 7, 8, 0])
    three = puzzle.make_star([9, 10, 11, 12, 0])
    four = puzzle.make_star([13, 14, 15, 0])
    return one, two, three, four


def contains(child):
    for state in visited[child % visited_count]:
        if state == child:
            return True
    return False


visited = []
data = []
visited_count = 1000
for i in range(visited_count):
    visited.append([])


def move_on_bfs(start):
    fringe = [start]

    while len(fringe) != 0:
        state = fringe.pop(0)
        _ = state.state_to_hash()
        if contains(_):
            continue
        visited[_ % visited_count].append(_)
        data.append(state.state_to_data_string())
        for child in state.get_children():
            if not contains(child.state_to_hash()):
                fringe.append(child)


init_state = HereMap.make_final_state(4)
one, two, three, four = make_it_4(init_state)
move_on_bfs(one)
print(len(data))
f = open('disjoint_one.txt', '+w')
for item in data:
    f.writelines([item, "\n"])
f.close()
data = []
move_on_bfs(two)
print(len(data))
f = open('disjoint_two.txt', '+w')
for item in data:
    f.writelines([item, "\n"])
f.close()
data = []
move_on_bfs(three)
print(len(data))
f = open('disjoint_three.txt', '+w')
for item in data:
    f.writelines([item, "\n"])
f.close()
data = []
move_on_bfs(four)
print(len(data))
f = open('disjoint_four.txt', '+w')
for item in data:
    f.writelines([item, "\n"])
f.close()
