part_count = 1000


def make_it_4(puzzle):
    one = puzzle.make_star([1, 2, 3, 4, 0])
    two = puzzle.make_star([5, 6, 7, 8, 0])
    three = puzzle.make_star([9, 10, 11, 12, 0])
    four = puzzle.make_star([13, 14, 15, 0])
    return one, two, three, four


class DisjointSet:

    def __init__(self, files):
        temp = [42, 42, 45, 46]
        self.states = []
        for files_number in range(len(files)):
            f = open(files[files_number], "r+")
            temp_list = []
            for i in range(part_count):
                temp_list.append(list())
            for line in f:
                state = line[0:temp[files_number]]
                cost = line[temp[files_number] + 1:-1].split(',')[files_number]
                _ = state.__hash__()
                temp_list[_ % part_count].append(MyMapStar(_, cost))
            f.close()
            self.states.append(temp_list)

    def find(self, state):
        sum = 0
        i = 0
        for temp_state in make_it_4(state):
            temp_hash = temp_state.state_to_old_hash()
            temp_list = self.states[i][temp_hash % part_count]
            i += 1
            for item in temp_list:
                if item.is_equal(temp_hash):
                    sum += item.cost
                    break
        return sum


class MyMapStar:

    def __init__(self, state, cost):
        self.cost = int(cost)
        self.state = state

    def is_equal(self, other):
        if self.state == other:
            return True
        else:
            return False
